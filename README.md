# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Chatroom
* Key functions (add/delete)
    1. One-on-one chat
    2. Group chat
    3. Load message history
    4. Chat with new user
    5. Dialog bubbles
    
* Other functions (add/delete)
    1. Add group
    2. Forgot password
    3. User profile
    4. Change email
    5. Premium account

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midterm-project-d1014.firebaseapp.com

* Website overview
<img src="./overview.png">
* Website overview (mobile)
<img src="./overview_m.jpg">

# Components Description : 
1. One-on-one chat : 
    > 登入後，可以透過搜尋他人的email address進行一對一聊天。若是無效用戶，則跳出alert警示。
2. Group chat : 
    > 登入後，可以透過搜尋已被創建的群組名稱(創建群組的方法詳見Other Functions → Add group)，與所有群組內的成員進行多人聊天。若是無效群組，或者你不是該群組的成員，則跳出alert警示。
3. Load message history : 
    > 進入聊天室時，會顯示先前的所有聊天記錄。無論該聊天室內的任何一人發布新訊息，聊天記錄都會realtime更新。
4. Chat with new user : 
    > 每次新註冊一個user，我都會將他們的email address存入<code>firebase.database().ref('user_list')</code>中。
    > 因此就算是新用戶，也可以成功地搜尋其他用戶/被搜尋，享有與舊用戶相等的聊天功能。
5. Dialog bubbles : 
    > 在聊天室中，如果是自己發的訊息，對話泡泡會顯示在右側，其餘的會顯示在左側，並顯示發布者的email address。除此之外，每個對話泡泡都會顯示發布時間。每次聊天室有新訊息，聊天室都會自動捲動至底部。

# Other Functions Description(1~10%) : 
1. Add group : 
    > 登入後，按下工具列的Add Group按鈕，可以進入創建群組的頁面。輸入群組名稱、群組成員(email)並按Create Group後，會將**群組名稱**、**user.uid**和**輸入的有效用戶的UID** push進<code>firebase.database().ref('group_list')</code>中。
2. Forgot password : 
    > 在**signin.html**中，可以按下forgot password的超連結，在裡面輸入email address並按下按鈕後，藉由<code>sendPasswordResetEmail()</code>，如果是有效的email address，該電子信箱就會收到一則信件，裡面有變更密碼的超連結。
3. User profile : 
    > 登入後，點擊Set Profile按鈕，可以進入個人資料頁面，裡面包含UID(唯讀)、Email、Credit Card、Security Setting(後來沒有用到)。除了UID，使用者可以自由更改其他資料，並在按下Save Change後，藉由<code>update()</code>更新資料。
4. Change email : 
    > 承User profile，按下Save Change後，藉由<code>updateEmail()</code>更新Authentication中的資料，未來登入時使用的email address便會被更新。
5. Premium account : 
    > 承User profile，按下Save Change後，會更新該使用者的信用卡卡號。為求簡便，只要內容不是空字串，系統便會認定使用者是尊貴會員。尊貴會員的主頁面背景會是金色，並顯示**You are premium now!**。

## Security Report (Optional)
1. 在database rules中加入<code>auth != null</code>，必須是通過authentication的用戶，才能read/write database裡的資料。
2. 進入**index.html**時，先用<code>firebase.auth().onAuthStateChanged</code>判斷用戶是否已登入，若未登入，工具欄將不會顯示搜尋用戶的文字輸入欄位，以及Chat/Add Group/Set Profile按鈕，進一步禁止未登入用戶read/write database裡的資料。
3. 我將聊天訊息push進<code>firebase.database().ref('com_list')</code>時，每一筆key都包含一個child：<code>accessors</code>，即**可以取用這則訊息的人**。<br>
<code>accessors[0]</code>存取**訊息發送者的uid**，<code>accessors[1]</code>存取**接收者的uid**(若是群組訊息，則是存取**群組名稱**)。<br>
一對一：顯示聊天記錄時，訊息屬於此聊天室只有兩種情況：
    * <code>accessors[0]</code>是user.uid，<code>accessors[1]</code>是聊天對象的uid(自己的訊息)
    * <code>accessors[0]</code>是聊天對象的uid，<code>accessors[1]</code>是user.uid(聊天對象的訊息)

    群組：顯示聊天記錄時，訊息屬於此聊天室只有兩種情況：
    * <code>accessors[0]</code>是user.uid，<code>accessors[1]</code>是群組名稱(自己的訊息)
    * <code>accessors[0]</code>不是user.uid，<code>accessors[1]</code>是群組名稱(群組成員的訊息)

    因為每個帳號的uid是獨一無二的，我們便能藉由比較<code>accessors</code>與自己的uid、聊天對象的uid，進而侷限顯示於聊天室中的訊息，避免使用者取用自己沒有權限的聊天室的訊息。
4. 使用<code>htmlspecialchars()</code>，在push訊息時，將訊息中的特殊字元替換掉，防範使用者輸入html tags，攻擊聊天室。

## Image credit
* icons: https://www.flaticon.com/
* background: https://www.pinterest.com/pin/507569820478783120/